package top.natsu.reminder;

import javafx.application.Application;
import top.natsu.reminder.gui.MyWindows;

/**
 * @author shikanatsu
 * @date 2022/5/19 0019 11:00
 **/
public class AppMain {

    public static void main(String[] args) {
        Application.launch(MyWindows.class,args);
    }

}
