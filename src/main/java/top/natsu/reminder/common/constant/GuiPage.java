package top.natsu.reminder.common.constant;

/**
 * @Description
 * @Author shikanatsu
 * @Date 2022/4/2 0002 10:51
 **/
public enum GuiPage {

    IndexPage("/view/IndexLayout.fxml", "top.natsu.reminder.gui.controller.IndexController"),
    ReportPage("/view/ReportLayout.fxml", "top.natsu.reminder.gui.controller.ReportController"),
    ConfigPage("/view/ConfigLayout.fxml", "top.natsu.reminder.gui.controller.ConfigController"),
    AboutPage("/view/AboutLayout.fxml", "top.natsu.reminder.gui.controller.AboutController");
//    SuperiorPage("/view/SuperiorLayout.fxml", "top.natsu.reminder.gui.controller.SuperiorController");


    String path;

    String controllerName;

    GuiPage(String path, String clz) {
        this.path = path;
        this.controllerName = clz;
    }

    public String getPath() {
        return path;
    }


    public String getControllerName() {
        return controllerName;
    }
}
