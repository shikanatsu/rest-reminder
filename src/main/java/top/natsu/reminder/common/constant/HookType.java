package top.natsu.reminder.common.constant;/**
 * @author shikanatsu
 * @create 2022/3/28 0028 18:26
 */

/**
 * @Description 钩子类型
 * @Author shikanatsu
 * @Date 2022/3/28 0028 18:26
 *
 **/
public enum HookType {

    KEYBOARD,MOUSE,PROCESS

}
