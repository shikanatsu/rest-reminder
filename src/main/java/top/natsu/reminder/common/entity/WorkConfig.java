package top.natsu.reminder.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import top.natsu.reminder.common.enums.TimeType;

import java.io.Serializable;

/**
 * @description 工作配置/  后续可能适配多种配置
 * @author shikanatsu
 * @date 2022/3/29 0029 11:42
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkConfig implements Serializable {

    /**
     * 工作时间
     **/
    private Integer workTime = 45;

    private TimeType restTimeUnit;

    /**
     * 休息时间
     **/
    private Integer restTime = 5;

    private String tips = "该休息了~";

    private boolean showFullTips = true;

    private boolean simpleTips = false;

    private Integer againTime = 5;




}
