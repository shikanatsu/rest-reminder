package top.natsu.reminder.common.enums;

import java.util.concurrent.TimeUnit;

/**
 * @Description
 * @Author shikanatsu
 * @Date 2022/3/31 0031 14:33
 **/
public enum TimeType {
    /** **/
    MINUTES(1, TimeUnit.MINUTES,"分钟"),

    SECONDS(2, TimeUnit.SECONDS,"秒");

    private Integer code;

    private TimeUnit timeUnit;

    private String name;

    TimeType(Integer code, TimeUnit timeUnit, String name) {
        this.code = code;
        this.timeUnit = timeUnit;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static TimeType getTimeByName(String name) {
        TimeType[] values = TimeType.values();
        for (TimeType item : values) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return TimeType.MINUTES;
    }

    public Integer getCode() {
        return code;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public static TimeType getTimeType(Integer code) {

        TimeType[] values = TimeType.values();
        for (TimeType item : values) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        return TimeType.MINUTES;
    }
}
