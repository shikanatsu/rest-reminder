package top.natsu.reminder.gui;

import cn.hutool.core.lang.Console;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import top.natsu.reminder.common.constant.GuiPage;
import top.natsu.reminder.gui.controller.IndexController;
import top.natsu.reminder.process.TrayHandler;
import top.natsu.reminder.util.ApplicationHolderUtil;

import java.io.IOException;

/**
 * @Description GUI 窗口
 * @Author shikanatsu
 * @Date 2022/3/29 0029 14:43
 **/
public class MyWindows extends Application {

    private Stage primaryStage;

    private final static String indexIcon = "/static/images/fish.png";


    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Rest-reminder V1.3.0");
        primaryStage.getIcons().add(new Image(MyWindows.class.getResourceAsStream(indexIcon)));
        primaryStage.setResizable(false);
        initRootLayout();
        TrayHandler.getInstance().listen(primaryStage);
        ApplicationHolderUtil.addStage("main", primaryStage);
        TrayHandler.getInstance().hide(ApplicationHolderUtil.getStage("main"));
//        System.out.println("Default Charset=" + Charset.defaultCharset());
//        System.out.println("file.encoding=" + System.getProperty("file.encoding"));
//        System.out.println("Default Charset=" + Charset.defaultCharset());
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyWindows.class.getResource(GuiPage.IndexPage.getPath()));
            // Show the scene containing the root layout.
            Scene scene = new Scene(loader.load());
            //Notice the order of execution ,need load node
            ApplicationHolderUtil
                    .addWindow(IndexController.class.getName(), loader.getController());
            primaryStage.setScene(scene);
//            primaryStage.show();
        } catch (IOException e) {
            Console.log("初始化界面错误", e);
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        MyWindows.launch(args);
    }

}
