package top.natsu.reminder.gui;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Console;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import top.natsu.reminder.cron.ClockActuator;
import top.natsu.reminder.gui.controller.RestController;
import top.natsu.reminder.handler.TimeRecord;
import top.natsu.reminder.util.AlertUtil;
import top.natsu.reminder.util.ApplicationHolderUtil;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * @description 休息界面窗口创建
 * 全屏锁定参考代码 : https://www.it1352.com/980011.html
 * @author shikanatsu
 * @date 2022/3/30 0030 14:32
 **/
public class RestLockWindows {


    private static final String restFxml = "/view/RestLayout.fxml";

    public static Stage stage;

    private static RestController restController;

    private static boolean isRest = false;


    public static Stage doRest(String label, Integer restTime,
                               TimeUnit timeUnit,boolean isSimpleTips) {
        if(isSimpleTips){
            AlertUtil.alert(Alert.AlertType.INFORMATION,"生活需要些许停顿,注意休息~",label);
            return null;
        }
        stage = newStage();
        ApplicationHolderUtil.addStage("rest", stage);
        // Load root layout from fxml file.
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MyWindows.class.getResource(restFxml));
        try {
            AnchorPane rootLayout = loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            stage.setScene(scene);
            //是否可拖动
            stage.setResizable(true);
            //set default label
            restController = loader.getController();
            restController.getTipContent().setText(label);
            stage.show();
        } catch (Exception e) {
            Console.log("error" + e);
        }
        countDown(restTime, timeUnit);
        return stage;
    }


    private static void countDown(Integer restTime, TimeUnit timeUnit) {
        isRest = true;
        CompletableFuture.runAsync(() -> {
            Date date = new Date();
            long endTime;
            switch (timeUnit) {
                case SECONDS:
                    endTime = DateUtil.offsetSecond(date, restTime).getTime();
                    break;
                case MINUTES:
                    endTime = DateUtil.offsetMinute(date, restTime).getTime();
                    break;
                default:
                    return;
            }
            long startTime = date.getTime();
            long midTime = (endTime - startTime) / 1000;
            while (isRest && midTime > 0) {
                midTime--;
                long hh = midTime / 60 / 60 % 60;
                long mm = midTime / 60 % 60;
                long ss = midTime % 60;
                String label = "休息还剩" + hh + "小时" + mm + "分钟" + ss + "秒";
                restController.getCountdownLabel().setText(label);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            ClockActuator.latch.countDown();
            stage.close();
        });
    }


    private static Stage newStage() {
        // 创建新的stage
        Stage newStage = new Stage(StageStyle.UNIFIED);
//        newStage.setWidth(300);
        newStage.setAlwaysOnTop(true);
        newStage.setFullScreen(true);
        newStage.fullScreenProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue) {
                newStage.setFullScreen(true);
            }
        });
        newStage.setOnCloseRequest((event) -> {
            //手动关闭窗口通知重置
            isRest = false;
            TimeRecord.getInstance().setNextRestTime(DateUtil.offsetMinute(new Date(), TimeRecord.getInstance().getWorkConfig().getAgainTime()));
            stage.close();
            ClockActuator.latch.countDown();
        });
        newStage.setHeight(200);
        return newStage;
    }


}
