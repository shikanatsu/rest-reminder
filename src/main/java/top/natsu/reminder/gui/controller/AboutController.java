package top.natsu.reminder.gui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.ImageView;

/**
 * @Description TODO
 * @Author shikanatsu
 * @Date 2022/4/2 0002 11:40
 **/
public class AboutController {



    @FXML
    private ImageView payQr;

    @FXML
    private ImageView wxQr;

    @FXML
    private ImageView authorHead;

    @FXML
    private Hyperlink blogUrl;

    @FXML
    private Hyperlink gitUrl;

    @FXML
    void openUrl(ActionEvent event) {
        //// TODO: 2022/4/2 0002 打开超链接
    }


}
