package top.natsu.reminder.gui.controller;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Console;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import top.natsu.reminder.common.constant.GuiPage;
import top.natsu.reminder.common.entity.WorkConfig;
import top.natsu.reminder.cron.ClockActuator;
import top.natsu.reminder.handler.TimeRecord;
import top.natsu.reminder.monitor.Monitor;
import top.natsu.reminder.util.ApplicationHolderUtil;
import top.natsu.reminder.util.ConfigFileUtil;

import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @Description V2主页
 * @Author shikanatsu
 * @Date 2022/4/1 0001 17:46
 **/
public class IndexController implements Initializable {


    @FXML
    private VBox leftVbox;

    @FXML
    private AnchorPane rightPane;

    @FXML
    private Button reportButton;

    @FXML
    private Button configButton;


    @FXML
    private Text workStatus;

    /**
     * 高级
     **/
    @FXML
    private Button superiorButton;

    @FXML
    private Button aboutButton;


    @FXML
    private Text tiredValue;

    @FXML
    private Text runTime;

    private WorkConfig workConfig;

    private Map<GuiPage, Node> loaderResource = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        changePane(GuiPage.ReportPage);
        // 自动启动_ 初始化加载默认配置
        workConfig = ConfigFileUtil.reloadLocalConfig();
        //auto start
        start();
    }


    @FXML
    void onClickReport(ActionEvent event) {
        changePane(GuiPage.ReportPage);
    }

    @FXML
    void onClickConfigButton(ActionEvent event) {
        changePane(GuiPage.ConfigPage);
    }

    @FXML
    void onClickExpert(ActionEvent event) {
//        changePane(GuiPage.SuperiorPage);
    }

    @FXML
    void onClickAbout(ActionEvent event) {
        changePane(GuiPage.AboutPage);
    }


    private boolean changePane(GuiPage guiPage) {
        Assert.notNull(guiPage);
        Node ld = this.loaderResource.get(guiPage);
        if (null == ld) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(IndexController.class.getResource(guiPage.getPath()));
            try {
                ld = loader.load();
                this.loaderResource.put(guiPage, ld);
                ApplicationHolderUtil.addWindow(guiPage.getControllerName(), loader.getController());
            } catch (Exception e) {
                Console.log("页面资源加载失败", e);
            }
        }
        if (null == ld) {
            return false;
        }
        this.rightPane.getChildren().clear();
        this.rightPane.getChildren().add(ld);
        return true;
    }

    public void setFatigueText(Integer value) {
        tiredValue.setText(value.toString());
    }

    public void start() {
        //start monitor
        TimeRecord timeRecord = TimeRecord.getInstance().bindWorkConfig(workConfig);
        timeRecord.setStartTime(new Date());
        new Monitor(timeRecord);
        ClockActuator.startTimer();
        Console.log("自动启动完成");
        workStatus.setText("运行中");
//        AlertUtil.alert(Alert.AlertType.INFORMATION, "开始工作啦~", "冲冲冲~ 已经启动啦~ 记得休息");
//        TrayHandler.getInstance().hide(ApplicationHolderUtil.getStage("main"));
    }


    public void setReportTime(String workTime, Integer fatigue) {
        this.runTime.setText(workTime);
        this.tiredValue.setText(fatigue.toString());
    }



}
