package top.natsu.reminder.gui.controller;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import top.natsu.reminder.common.entity.WorkConfig;
import top.natsu.reminder.cron.ClockActuator;
import top.natsu.reminder.handler.TimeRecord;
import top.natsu.reminder.monitor.Monitor;
import top.natsu.reminder.process.TrayHandler;
import top.natsu.reminder.util.AlertUtil;
import top.natsu.reminder.util.ApplicationHolderUtil;


/**
 * @Author shikanatsu
 * @Date 2022/3/30 0030 12:23
 **/
@Deprecated
public class IndexControllerV1 {
    @FXML
    private TextField restTime;

    @FXML
    private Button start;

    @FXML
    private TextField workTime;

    @FXML
    private Text fatigueText;

    @FXML
    private ChoiceBox<String> timeType;

    @FXML
    void StartWork(ActionEvent event) {

        //check info
        String wTime = workTime.getText();
        String rTime = restTime.getText();
        String type = timeType.getValue();
        if (StrUtil.isBlank(wTime) || StrUtil.isBlank(rTime)) {
            AlertUtil.alert(Alert.AlertType.ERROR, "配置错误", "请正确填写配置");
            return;
        }

        if (!NumberUtil.isNumber(wTime) || !NumberUtil.isNumber(rTime)) {
            AlertUtil.alert(Alert.AlertType.ERROR, "配置错误", "时间必须为数值类型");
            return;
        }
        if (ClockActuator.isRunning()) {
            AlertUtil.alert(Alert.AlertType.ERROR, "靓仔别点这么多次,已经启动了的", "已经启动别点了！");
            return;
        }
        //start
        TimeRecord timeRecord = TimeRecord.getInstance().bindWorkConfig(new WorkConfig());
        new Monitor(timeRecord);
        ClockActuator.startTimer();
        AlertUtil.alert(Alert.AlertType.INFORMATION, "开始工作啦~", "冲冲冲~ 已经启动啦~ 记得休息");
        TrayHandler.getInstance().hide(ApplicationHolderUtil.getStage("main"));
    }

    public void setFatigueText(String vl){
        fatigueText.setText(vl);
    }
}
