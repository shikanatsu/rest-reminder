package top.natsu.reminder.gui.controller;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import top.natsu.reminder.util.AlertUtil;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * @Description
 * @Author shikanatsu
 * @Date 2022/4/1 0001 16:22
 **/
public class ReportController implements Initializable {


    @FXML
    private CategoryAxis xAxis;

    @FXML
    private javafx.scene.chart.NumberAxis yAxis;

    @FXML
    private LineChart lineChart;


    private XYChart.Series series;

    private Date lastRecordTime;

    @FXML
    private Button resetReport;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        xAxis.setLabel("时间");
        yAxis.setLabel("疲劳值");
//        yAxis.setLowerBound(0);
        yAxis.setUpperBound(50);
        //刻度线
        yAxis.setTickUnit(5);
        Date date = new Date();
        //添加分类
//        xAxis.getCategories().add(calculateTimeStr(date));
        series = new XYChart.Series();
        series.getData().add(new XYChart.Data(calculateTimeStr(date), 0));
        lineChart.getData().add(series);
    }


    public void putXyData(Date date, int value, boolean dMode) {
        ObservableList data = series.getData();
        boolean ref = dMode || null == lastRecordTime || data.size() < 10 || (data.size() > 8 && DateUtil.between(date, lastRecordTime, DateUnit.MINUTE) >= 5);
        if (ref) {
            String keyword = calculateTimeStr(date);
            series.getData().add(new XYChart.Data(keyword, value));
            lastRecordTime = date;
        }
    }


    private String calculateTimeStr(Date date) {
        String lowTime = DateUtil.format(date, "HH:mm");
        return lowTime;
    }


    public void resetData() {
        series.getData().clear();
//        lineChart.getData().clear();
        series.getData().add(new XYChart.Data(calculateTimeStr(new Date()), 0));
//        lineChart.getData().add(series);
    }

    @FXML
    void onClickRestReport(ActionEvent event) {
        resetData();
        AlertUtil.alert(Alert.AlertType.INFORMATION,"重置报表","重置报表成功");
    }


}
