package top.natsu.reminder.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import lombok.Data;

/**
 * @Description 休息界面
 * @Author shikanatsu
 * @Date 2022/3/30 0030 18:13
 **/
@Data
public class RestController  {


    @FXML
    private Label tipContent;


    @FXML
    private Text countdownLabel;


}
