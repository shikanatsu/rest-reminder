package top.natsu.reminder.util;

import javafx.scene.control.Alert;

/**
 * @Description GUi 弹窗工具 https://code.makery.ch/blog/javafx-dialogs-official/
 * @Author shikanatsu
 * @Date 2022/3/30 0030 14:24
 **/
public class AlertUtil {


    public static void alert(Alert.AlertType alertType,String title,String  contentText) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(contentText);
        alert.showAndWait();
    }
}
