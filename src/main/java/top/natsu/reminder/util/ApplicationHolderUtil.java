package top.natsu.reminder.util;

import javafx.stage.Stage;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description 临时保存舞台窗口
 * @Author shikanatsu
 * @Date 2022/3/30 0030 18:34
 **/
public class ApplicationHolderUtil {


    private static ConcurrentHashMap<String, Stage> stages;

    private static ConcurrentHashMap<String, Object> windows;



    static {
        stages = new ConcurrentHashMap<>();
        windows = new ConcurrentHashMap<>();
    }

    public static void addStage(String stageName, Stage stage) {
        stages.put(stageName, stage);
    }


    public static Stage getStage(String stageName) {
        return stages.get(stageName);
    }

    public static void addWindow(String name, Object obj) {
        if (null == obj) {
            return;
        }
        windows.put(name, obj);
    }

    public static <T> T getWindow(Class<T> clazz) {
        return clazz.cast(windows.get(clazz.getName()));
    }



}
