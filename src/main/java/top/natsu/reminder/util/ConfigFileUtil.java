package top.natsu.reminder.util;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import top.natsu.reminder.common.entity.WorkConfig;
import top.natsu.reminder.common.enums.TimeType;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @Description 配置文件工具
 * @Author shikanatsu
 * @Date 2022/4/2 0002 17:55
 **/
public class ConfigFileUtil {

    private static final String configPath = System.getProperty("user.dir") + "/config/config.json";


    public static WorkConfig reloadLocalConfig() {
        File file = new File(configPath);
        if (file.exists()) {
            String configFile = IoUtil.read(FileUtil.getInputStream(file)).toString(StandardCharsets.UTF_8);
            try {
                JSONObject jsonObject = JSONUtil.parseObj(configFile);
                WorkConfig newConfig = JSONUtil.toBean(configFile, WorkConfig.class);
                newConfig.setRestTimeUnit(TimeType.valueOf((String) jsonObject.get("restTimeUnit")));
                Console.log("初始化配置加载成功", newConfig);
                return newConfig;
            } catch (Exception e) {
                Console.log("配置刷新错误:{}", e);
            }
        }
        Console.log("未读取到配置,采用默认模式");
        return new WorkConfig();
    }


    public static void createConfigFile(WorkConfig config) {
        File file = new File(configPath);
        Console.log(file.exists());
        if (!FileUtil.exist(file)) {
            FileUtil.mkParentDirs(file);
            FileUtil.touch(file);
        }
        FileWriter.create(file, StandardCharsets.UTF_8).write(JSONUtil.toJsonStr(config));
        Console.log("配置保存成功");
    }


}
